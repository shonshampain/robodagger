package xx.robodagger;

import android.app.Application;

public class App extends Application {

    static RoboDaggerComponent roboDaggerComponent;
    static RoboDaggerComponent getComponent() {
        return roboDaggerComponent;
    }

    @Override public void onCreate() {
        super.onCreate();
        setupComponent();
    }

    protected void setupComponent() {
        roboDaggerComponent = DaggerRoboDaggerComponent.builder()
                .roboDaggerModule(new RoboDaggerModule())
                .build();
    }
}
