package xx.robodagger;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules={RoboDaggerModule.class})
interface RoboDaggerComponent {

    void inject(MainActivity mainActivity);
}
