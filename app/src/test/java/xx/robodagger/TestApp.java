package xx.robodagger;

public class TestApp extends App {

    @Override
    protected void setupComponent() {
        roboDaggerComponent = DaggerRoboDaggerComponent.builder()
                .roboDaggerModule(new TestRoboDaggerModule())
                .build();
    }

}
