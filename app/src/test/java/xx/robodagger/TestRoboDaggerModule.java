package xx.robodagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@SuppressWarnings("unused")
@Module
class TestRoboDaggerModule extends RoboDaggerModule {

    @Provides
    @Singleton
    Foo providesFoo() { return new FakeFoo(); }
}
